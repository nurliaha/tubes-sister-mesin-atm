# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer
# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler
import threading
import math
# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
data = []
data.append([1,123,100000])
data.append([2,1234,100000])
data.append([3,1235,100000])

emoney = []
emoney.append([123,10000])
emoney.append([456,10000])
emoney.append([789,10000])
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)
# Buat server
with SimpleXMLRPCServer(("172.20.10.5", 54321),
                        requestHandler=RequestHandler, allow_none=True) as server:
    server.register_introspection_functions()
    # siapkan lock
    lock = threading.Lock()

    # Nuliah Awaliah
    def menu():
        lock.acquire()
        lock.release()
        msg="==============================\n==  MESIN ATM  ==\n==============================\nMenu : \n1. Transfer\n2. Deposit Tunai\n3. Cek Saldo\n4. Penarikan\n5. Cek Mutasi\n6. Ganti PIN\n7. TopUp E-Money\n8. ganti ID\n9. FAQ"
        return msg
        lock.release()
    server.register_function(menu)

    #Hendro Susanto (Fitur Cek Mutasi Terakhir) -> Fitur Tambahan (COTS)
    def cekmutasi(id,pin):
        lock.acquire()
        lock.release()
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
        if (idx==-1):
            return "Maaf pin salah"
        else:
            return "Transaksi Terakhir Anda\n"+data[idx][3]
        lock.release()
    server.register_function(cekmutasi)

    #Andi Yuliah Haryono (Fitur Ganti PIN) -> Fitur Tambahan (COTS)
    def gantipin(id, pin,pin2):
        lock.acquire()
        lock.release()
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
        if (idx==-1):
            return "Maaf pin salah"
        else:
            data[idx][1] = pin2
            return "pin anda berhasil diganti"
        lock.release()
    server.register_function(gantipin)

    # Nurliah Awaliah (Fitur Pembayaran E-Money) -> Fitur Tambahan (COTS)
    def pembayaranemoney(id,pin,jumlah,idemoney):
        lock.acquire()
        lock.release()
        idm= -1
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
        for j in range(len(emoney)):
            if (emoney[j][0]==idemoney):
                idm=j
        if (idx==-1):
            return "Maaf password salah"
        elif (jumlah > data[idx][2]):
            return "maaf saldo tidak cukup"
        else:
            if (idm==-1):
                return "emoney belum terdaftar"
            else:
                data[idx][2]=data[idx][2]-jumlah
                emoney[idm][1]=emoney[idm][1]+jumlah
                return "top up berhasil \nsaldo e-money anda "+str(emoney[idm][1])
        lock.release()
    server.register_function(pembayaranemoney)

    #Muhammad Baihaqy (Fitur Ganti Nomor Rekening) -> Fitur Tambahan (COTS)
    def gantinorek(id, pin,id2):
        lock.acquire()
        lock.release()
        idx = -1
        j = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
            if (data[i][0]==id2):
                j+=1
        if (idx==-1):
            return "Maaf pin salah"
        elif (j==-1):
            data[idx][0]=id2
            return "id berhasil diganti"
        else:
            return "id sudah terdaftar"
        lock.release()
    server.register_function(gantinorek)

    #Muhammad Ihsan (Fitur Frequently Ask Question) ->Fitur Tambahan (COTS)
    def faq(a):
        lock.acquire()
        lock.release()
        if (a==1):
            return "tidak terbatas"
        elif (a==2):
            return "masih, untuk transaksi-transaksi non tunai"
        elif (a==3):
            return "lebih dari 18.000 ATM yang tersebar di seluruh pelosok negeri"
        elif (a==4):
            return "dapat menanyakan ke Cabang, atau lihat di website"
        elif (a==5):
            return "nasabah dapat mendatangi Cabang terdekat"
        lock.release()
    server.register_function(faq)

    # Nuliah Awaliah
    def ceksaldo(id, pin):
        lock.acquire()
        lock.release()
        saldo = -1
        for x in data:
            if (x[0]==id) and (x[1]==pin):
                saldo=x[2]
        if (saldo==-1):
            return "Maaf pin salah"
        else:
            return "SALDO ANDA : "+str(saldo)
        lock.release()
    server.register_function(ceksaldo)

    # Muh. Baihaqy
    def login(id, pin):
        lock.acquire()
        lock.release()
        user = -1
        for x in data:
            if (x[0]==id) and (x[1]==pin):
                user=1
        if (user==-1):
            return False
        else:
            return True
        lock.release()
    server.register_function(login)

    # Muh. Ihsan
    def tariktunai(id, pin,jumlah):
        lock.acquire()
        lock.release()
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
        if (idx==-1):
            return "Maaf password salah"
        else:
            if (jumlah > data[idx][2]):
                return "maaf saldo tidak cukup"
            else:
                data[idx][2]=data[idx][2]-jumlah
                return "berhasil \nsisa saldo "+str(data[idx][2])
        lock.release()
    server.register_function(tariktunai)

    # Andi Yulia
    def depo(id, pin,jumlah):
        lock.acquire()
        lock.release()
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
        if (idx==-1):
            return "Maaf password salah"
        else:
            data[idx][2]=data[idx][2]+jumlah
            return "berhasil \ntotal saldo "+str(data[idx][2])
        lock.release()
    server.register_function(depo)

    # Muh. Ihsan
    def transfer(id,pin,jumlah,idtujuan):
        lock.acquire()
        lock.release()
        idxt= -1
        idx = -1
        for i in range(len(data)):
            if (data[i][0]==id) and (data[i][1]==pin):
                idx=i
            if (data[i][0]==idtujuan):
                idxt=i
        if (idx==-1):
            return "Maaf password salah"
        else:
            if (jumlah > data[idx][2]):
                return "maaf saldo tidak cukup"
            else:
                if (idxt==-1):
                    return "maaf nomer rekening tujuan salah"
                else:
                    data[idx][2]=data[idx][2]-jumlah
                    data[idxt][2]=data[idxt][2]+jumlah
                    data[idx][3]="Transfer sejumlah Rp."+str(jumlah)+" ke nomor rekening "+str(idtujuan)
                    data[idxt][3]="Terima sejumlah Rp."+str(jumlah)+" dari nomor rekening "+str(id)
                    return "transfer berhasil \nsisa saldo "+str(data[idx][2])
        lock.release()
    server.register_function(transfer)

    print("Server CAOL Running...")
    # Jalankan server
    server.serve_forever()
