# Tubes-Sister-Mesin-ATM
Anggota Kelompok :

- Andi Yulia Haryono
- Hendro Susanto
- Muhammad Baihaqy
- Muhammad Ihsan
- Nurliah Awaliah


# Requirement
Python 3.6
# Langkah mendownload file
Sebelum menjalan kan aplikasi Mesin ATM  (Client Server) terlebih dahulu mendownload file pada Repository Tubes-Sister-Mesin-ATM. Setelah di upload running File Server.py (sebagai server pada Mesin ATM).
Setelah itu running file Client.py

# Running

# Server
Menjalankan file server.py pada terminal terlebih dahulu.

# Client
Kemudian menjalankan file client.py pada terminal lalu nasabah dapat menginputkan id dan pin atm pada halaman login.
Masukkan ip adress sesuai dengan ip server.